'use strict';

const line = require('@line/bot-sdk');
const express = require('express');
const config = require('./config.json');

// create LINE SDK client
const client = new line.Client(config);

const app = express();

// webhook callback
app.post('/webhook', line.middleware(config), (req, res) => {
  // req.body.events should be an array of events
  if (!Array.isArray(req.body.events)) {
    return res.status(500).end();
  }
  // handle events separately
  Promise.all(req.body.events.map(event => {
    console.log('event', event);
    // check verify webhook event
    if (event.source.userId === 'Udeadbeefdeadbeefdeadbeefdeadbeef') {
      return;
    }
    // if (event.source.userId === 'U8e478b5e4c4b32c091b88bef49bf550f') {
    //   return;
    // }
    return handleEvent(event);
  }))
    .then(() => res.end())
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
});

// simple reply function
const replyText = (token, text) => {
  const msg = { type: 'text', text };
  return client.replyMessage(token, msg);
}

async function handleEvent(event) {
  switch (event.type) {
    case 'message':
      const message = event.message;
      switch (message.type) {
        case 'text':
          return handleText(message, event);
        case 'image':
          return handleImage(message, event.replyToken);
        case 'video':
          return handleVideo(message, event.replyToken);
        case 'audio':
          return handleAudio(message, event.replyToken);
        case 'location':
          return handleLocation(message, event.replyToken);
        case 'sticker':
          return handleSticker(message, event.replyToken);
        default:
          throw new Error(`Unknown message: ${JSON.stringify(message)}`);
      }

    case 'follow':
      const profile = await client.getProfile(event.source.userId);
      return replyText(event.replyToken, `สวัสดีคุณ ${profile.displayName} ยินดีต้อนรับครับ`);

    case 'unfollow':
      return console.log(`Unfollowed this bot: ${JSON.stringify(event)}`);

    case 'join':
      return replyText(event.replyToken, `Joined ${event.source.type}`);

    case 'leave':
      return console.log(`Left: ${JSON.stringify(event)}`);

    case 'postback':
      let data = event.postback.data;
      return replyText(event.replyToken, `Got postback: ${data}`);

    case 'beacon':
      const dm = `${Buffer.from(event.beacon.dm || '', 'hex').toString('utf8')}`;
      const replyMessage = `${event.beacon.type} beacon hwid : ${event.beacon.hwid} with device message = ${dm}`;
      return replyText(event.replyToken, replyMessage);

    default:
      throw new Error(`Unknown event: ${JSON.stringify(event)}`);
  }
}

function handleText(message, event) {
  if (message.text === 'token') {
    const msg = {
      type: 'text',
      text: 'Channel access token="1WUug8bH5Dd8g58xRM1laA0fw3quJRGE7t5iXucNcJ9x91TMFAVbKMHCg0qKQfqUk/4+fgLMco+7mKxdGnYtInRNXlOhArZKjkmB1OtvFl87bOFGOLzF2i2c7WAP4mVfwMHmOOmGbba2RA4u1rcMyAdB04t89/1O/w1cDnyilFU=", channelAccessToken="c76b9662ebcda07b5a8c299a1d1ec6b0"'
    };
    // return replyText(replyToken, msg.text);
    // return client.replyMessage(replyToken, msg);
    console.log('event.source.userId', event.source.userId);
    return client.pushMessage(event.source.userId, msg);
    
  } else if (message.text === 'git') {
    const msg = {
      type: 'text',
      text: 'https://gitlab.com/nat.rojsawang/workshop-line-apartment.git'
    };
    return client.pushMessage(event.source.userId, msg);
    
  } else {
    return replyText(event.replyToken, message.text);
  }
}

function handleImage(message, replyToken) {
  return replyText(replyToken, 'Got Image');
}

function handleVideo(message, replyToken) {
  return replyText(replyToken, 'Got Video');
}

function handleAudio(message, replyToken) {
  return replyText(replyToken, 'Got Audio');
}

function handleLocation(message, replyToken) {
  return replyText(replyToken, 'Got Location');
}

function handleSticker(message, replyToken) {
  const msg = {
    type: 'sticker',
    packageId: 1,
    stickerId: 1
  };
  return replyText(replyToken, msg);
}

const port = config.port;
app.listen(port, () => {
  console.log(`listening on ${port}`);
});
